TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/Shoppe.cpp \
    src/Game.cpp \
    src/Error.cpp \
    src/Options.cpp \
    src/ui/Tab.cpp \
    src/ui/Panel.cpp \
    src/ui/Menu.cpp \
    src/ui/Keys.cpp \
    src/ui/Element.cpp \
    src/ui/Control.cpp \
    src/ui/Button.cpp \
    src/shop/Tile.cpp \
    src/shop/Square.cpp \
    src/shop/Portal.cpp \
    src/shop/Items.cpp \
    src/shop/Item.cpp \
    src/shop/Indicator.cpp \
    src/shop/Furniture.cpp \
    src/shop/Floor.cpp \
    src/screens/Screen.cpp \
    src/screens/MainMenuScreen.cpp \
    src/screens/GameScreen.cpp \
    src/rendering/Sprite.cpp \
    src/rendering/Shader.cpp \
    src/rendering/Quad.cpp \
    src/rendering/Painter.cpp \
    src/characters/Sid.cpp \
    src/characters/Player.cpp \
    src/characters/Characters.cpp \
    src/characters/Character.cpp \

HEADERS += \
    src/Options.h \
    src/Game.h \
    src/Error.h \
    src/ui/Tab.h \
    src/ui/Panel.h \
    src/ui/Menu.h \
    src/ui/Keys.h \
    src/ui/Element.h \
    src/ui/Control.h \
    src/ui/Button.h \
    src/shop/Tile.h \
    src/shop/Square.h \
    src/shop/Portal.h \
    src/shop/Items.h \
    src/shop/Item.h \
    src/shop/Indicator.h \
    src/shop/Furniture.h \
    src/shop/Floor.h \
    src/screens/Screen.h \
    src/screens/MainMenuScreen.h \
    src/screens/GameScreen.h \
    src/rendering/Stroke.h \
    src/rendering/Sprite.h \
    src/rendering/Shader.h \
    src/rendering/Quad.h \
    src/rendering/Painter.h \
    src/characters/Sid.h \
    src/characters/Player.h \
    src/characters/Characters.h \
    src/characters/Character.h

INCLUDEPATH += B:\include
LIBS += -LB:\lib -lglfw3 -lglew32 -lSOIL -lopengl32 -lglu32 -lgdi32 -lfreetype -lFTGL -lyaml-cpp

QMAKE_CXXFLAGS += -std=c++11

OTHER_FILES +=
