attribute vec2 xy; 
attribute vec2 st;
uniform mat4 matrix;
void main()
{
    gl_TexCoord[0] = vec4(st, 0.0, 1.0);
    gl_Position = matrix*vec4(xy, 0.0, 1.0);
}