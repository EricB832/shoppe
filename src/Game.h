/*
 * Game.h
 *
 *  Created on: Feb 18, 2013
 *      Author: eric
 */

#ifndef GAME_H_
#define GAME_H_

#include <string>
#include <memory>
#include "screens/Screen.h"
#include <GL/glfw3.h>

class Game {
public:
    static GLFWwindow* window;
    static void exit(GLFWwindow* window);
    static bool quit;

	static double delta();

	void open();
	void setup();
	void render();
	void run();
	void update();
	void transition(std::unique_ptr<Screen> s);


private:

	static double dt;
	std::unique_ptr<Screen> screen;
	std::unique_ptr<Screen> next;
};

#endif /* GAME_H_ */
