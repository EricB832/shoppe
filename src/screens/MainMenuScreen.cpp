/*
 * MainMenu.cpp
 *
 *  Created on: Feb 22, 2013
 *      Author: eric
 */

#include "MainMenuScreen.h"
#include "../ui/Button.h"
#include "../ui/Menu.h"
#include "GameScreen.h"
#include "../Game.h"
#include <memory>
#include <Gl/glfw3.h>
using namespace std;

vector<Control> main_menu_controls(Menu& menu);
vector<unique_ptr<Stroke>> main_menu_buttons(Game& game, Menu& menu);

MainMenuScreen::MainMenuScreen(Game& game){
	painters.emplace_back(new Menu);
	Menu& menu = static_cast<Menu&>(*painters[0]);

	controls.push(main_menu_controls(menu));
	menu.sketch(main_menu_buttons(game, menu));

	menu.set(0);
}

void MainMenuScreen::print(){
	((Menu*)painters[0].get())->print();
}

vector<unique_ptr<Stroke>> main_menu_buttons(Game& game, Menu& menu){
	vector<unique_ptr<Stroke>> buttons;

	buttons.emplace_back(new Button("Start", *menu.font,
		[&game](){
            game.transition(move(unique_ptr<Screen>(new GameScreen())));
		}));

	buttons.emplace_back(new Button("Exit" , *menu.font,
		[](){
            Game::exit(Game::window);
		}));

	return buttons;
}

vector<Control> main_menu_controls(Menu& menu){
	vector<Control> controls;
	controls.emplace_back(GLFW_KEY_DOWN,  [&menu](){menu.down();}, nullptr, nullptr);
	controls.emplace_back(GLFW_KEY_UP,    [&menu](){menu.up();}, nullptr, nullptr);
	controls.emplace_back(GLFW_KEY_ENTER, [&menu](){menu.press();}, nullptr, [&menu](){menu.release(); menu.activate();});
	return controls;
}
