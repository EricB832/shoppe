/*
 * GameScreen.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef GAMESCREEN_H_
#define GAMESCREEN_H_

#include "Screen.h"
#include "../Game.h"
#include "glm/glm.hpp"

class GameScreen: public Screen {
public:
    GameScreen();
	void translate(float x, float y);
	void print(){}
	void process();
};

#endif /* GAMESCREEN_H_ */
