/*
 * GameScreen.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#include "GameScreen.h"
#include "../Options.h"
#include "../shop/Floor.h"
#include "../characters/Characters.h"
#include "../ui/Menu.h"
#include "../Error.h"
#include "../shop/Portal.h"
#include "../shop/Items.h"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <string>
using namespace std;

vector<Control> calibrate_controls(stack<vector<Control>>& screen_controls, Menu& menu, Items& items);
vector<Control> game_controls(stack<vector<Control>>& screen_controls, Floor& floor, Characters& characters, Menu& menu, Items& items);

GameScreen::GameScreen() {
	painters.emplace_back(new Floor(7,7));
	Floor& floor = static_cast<Floor&>(*painters[0]);

	painters.emplace_back(new Characters(floor.indicator()));
	Characters& characters 	= static_cast<Characters&>(*painters[1]);

	painters.emplace_back(new Menu);
	Menu& menu = static_cast<Menu&>(*painters[2]);

	painters.emplace_back(new Items);
	Items& items = static_cast<Items&>(*painters[3]);

	controls.push(game_controls(controls, floor, characters, menu, items));

    glm::mat4 camera = glm::ortho<GLfloat>(0,Options::get<unsigned>(Options::WIDTH), 0, Options::get<unsigned>(Options::HEIGHT), -1, 1) *
                       glm::translate<GLfloat>(glm::mat4(1.0), glm::vec3((Options::get<unsigned>(Options::WIDTH)-floor.pixW())/2, (Options::get<unsigned>(Options::HEIGHT)-floor.pixH())/2, 0));

	for(auto& painter : painters){
		painter->sketch();
		painter->transform(camera);
	}
	menu.transform(glm::ortho<GLfloat>(0,1,0,1,-1,1));
}

vector<Control> game_controls(stack<vector<Control>>& screen_controls, Floor& floor, Characters& characters, Menu& menu, Items& items){
	Player& player = characters.player();
	vector<Control> controls;
	controls.emplace_back(player.movement(UP));
	controls.emplace_back(player.movement(LEFT));
	controls.emplace_back(player.movement(DOWN));
	controls.emplace_back(player.movement(RIGHT));
    controls.emplace_back(player.use(floor, items));
	controls.emplace_back('F', nullptr, nullptr,
		[&screen_controls, &player, &floor, &menu, &items](){
			if (!floor.valid(player.facing().i, player.facing().j)) return;
			auto& square = floor.get(player.facing().i, player.facing().j);
			if(square.object) {
				auto portal = dynamic_cast<Portal*>(square.object.get());
				if(portal) {
					portal->calibrate(menu, items, portal->era);
					screen_controls.push(calibrate_controls(screen_controls, menu, items));
				}
			}
		});
	return controls;
}

vector<Control> calibrate_controls(stack<vector<Control>>& screen_controls, Menu& menu, Items& items){
	vector<Control> controls;
	controls.emplace_back('F', nullptr, nullptr,
		[&screen_controls, &menu, &items](){
			menu.clear();
			items.pop();
			screen_controls.pop();
		});
	return controls;
}

