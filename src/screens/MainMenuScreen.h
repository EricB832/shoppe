/*
 * MainMenu.h
 *
 *  Created on: Feb 22, 2013
 *      Author: eric
 */

#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "Screen.h"
#include "../Game.h"

class MainMenuScreen: public Screen {
public:
	MainMenuScreen(Game& game);
	void print();
	~MainMenuScreen(){}
};

#endif /* MAINMENU_H_ */

