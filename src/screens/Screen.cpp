/*
 * Screen.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */
#include "Screen.h"

void Screen::update() {
	for(auto control : controls.top()){
		control.handle();
	}
	for(auto& painter : painters){
		painter->update();
	}
}
void Screen::render() {
	for(auto& painter : painters){
		painter->paint();
	}
	print();
}
void Screen::clear(){
	for(auto& painter : painters){
		painter->clear();
	}
}
