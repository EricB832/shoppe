/*
 * Keys.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: eric
 */

#include "Keys.h"
#include <map>
#include <iostream>
using namespace std;

map<int,bool> Keys::keys;

bool Keys::press(int key){
	if(keys.find(key)==keys.end() || !keys[key]) return keys[key] = true;
	return false;
}


bool Keys::release(int key){
	if(keys.find(key)!=keys.end() && keys[key]) return !(keys[key] = false);
	return false;
}
