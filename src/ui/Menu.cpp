/*
 * Menu.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */
#include <algorithm>
#include <linq.h>
#include "../Game.h"
#include "../Options.h"
#include "Button.h"
#include "Menu.h"
using namespace std;

map<string, string> Menu::tabs = {
		{"Tribal", "Military"},
		{"Iron", "Military"},
		{"Dark", "Military"},
		{"Renaissance", "Military"},
		{"Information", "Military"},
		{"Space", "Military"}
};

Menu::Menu():Painter("data/art/ui.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"), button(0),tab(0),font(new FTTextureFont(Options::get<std::string>(Options::MENU_FONT).c_str())){
    if(font->Error()) Game::exit(Game::window);
}

void Menu::set(unsigned a){
	button = a;
	((Button*)strokes[a].get())->highlight(a);
}

void Menu::press(){
	((Button*)strokes[button].get())->press(button);
}

void Menu::release(){
	set(button);
}

void Menu::up(){
	((Button*)strokes[button].get())->idle(button);
	button = button==0?strokes.size()-1:button-1;
	set(button);
}

void Menu::down(){
	((Button*)strokes[button].get())->idle(button);
	button = button+1>=strokes.size()?0:button+1;
	set(button);
}

void Menu::activate(){
	(*((Button*)strokes[button].get())).activate();
}

void Menu::print(){
	for (auto& s : strokes){
		((Button*) s.get())->print();
	}
}

vector<Button*> Menu::buttons(){
	vector<Button*> buttons = LINQ(from(stroke, strokes)
						where(dynamic_cast<Button*>(stroke.get()))
						select(dynamic_cast<Button*>(stroke.get()))) | linq::to_container;

	for(auto button : buttons){
		cout << button->text << endl;
	}

	return buttons;
}

