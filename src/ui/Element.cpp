/*
 * Element.cpp
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#include "Element.h"
using namespace std;
const YAML::Node Element::file = YAML::LoadFile("data/info/ui.yml");

Element::Element(vector<string> name):position(file, name) {}
vector<float> Element::vertices() const{
	return position.vertices();
}
