/*
 * Menu.h
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */

#ifndef MENU_H_
#define MENU_H_

#include <FTGL/ftgl.h>
#include "../rendering/Painter.h"
#include "Button.h"
#include <map>

class Menu : public Painter{
private:
	unsigned button;
	unsigned tab;

public:
	static std::map<std::string, std::string> tabs;
	Menu();
	void set(unsigned a);
	void press();
	void release();
	void up();
	void down();
	void activate();
	void print();
	std::vector<Button*> buttons();
	std::unique_ptr<FTFont> font;
};

#endif /* MENU_H_ */
