/*
 * Panel.h
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#ifndef PANEL_H_
#define PANEL_H_

#include "Element.h"
#include "../rendering/Sprite.h"
#include <string>

class Panel : public Element{
public:
	Panel(std::string panel);
	std::vector<float> coordinates() const;
private:
	Sprite sprite;
};

#endif /* PANEL_H_ */
