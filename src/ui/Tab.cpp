/*
 * Tab.cpp
 *
 *  Created on: Jul 22, 2013
 *      Author: Eric
 */

#include "Tab.h"
#include "../rendering/Quad.h"
#include <string>

using namespace std;

Tab::Tab(string tab):Element({"Tabs", tab}), active(file, {"Tabs", tab, "Active"}), inactive(file, {"Tabs", tab, "Inactive"}), selected(false){}

vector<float> Tab::coordinates() const {
	return selected ? active.coordinates() : inactive.coordinates();
}
