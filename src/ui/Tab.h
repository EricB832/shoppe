/*
 * Tab.h
 *
 *  Created on: Jul 22, 2013
 *      Author: Eric
 */

#ifndef TAB_H_
#define TAB_H_

#include "../ui/Element.h"
#include "../rendering/Sprite.h"
#include <vector>

class Tab : public Element {
private:
	Sprite active;
	Sprite inactive;
public:
	bool selected;
	Tab(std::string tab);
	std::vector<float> coordinates() const;
};

#endif /* TAB_H_ */
