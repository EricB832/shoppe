/*
 * Button.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#include "Button.h"
#include "../Options.h"
#include "../rendering/Painter.h"
#include <yaml-cpp/yaml.h>
#include <string>
#include <algorithm>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
using namespace std;


const std::string root = "Buttons";
const std::string Button::TEXT             = "Text";
const std::string Button::TEXT_COLOR       = "Text Color";
const std::string Button::RED              = "Red";
const std::string Button::GREEN            = "Green";
const std::string Button::BLUE             = "Blue";

Button::Button(std::string button, FTFont& font, std::function<void()> activate):
		Element({root,button}), activate(activate),
		state(0), font(font),
		idled(file, {root, button, "Idle"}),
		highlighted(file, {root, button, "Highlight"}),
		pressed(file, {root, button, "Press"}){
	parse(button);
}

void Button::idle(unsigned index){
	state = 0;
	change = true;
}

void Button::highlight(unsigned index) {
	state = 1;
	change = true;
}

void Button::press(unsigned index){
	state = 2;
	change = true;
}

int Button::process(){
	return 1;
}

void Button::parse(std::string button){
	text = file[root][button][TEXT].as<std::string>();
	r = file[root][button][TEXT_COLOR][RED].as<float>();
	g = file[root][button][TEXT_COLOR][GREEN].as<float>();
	b = file[root][button][TEXT_COLOR][BLUE].as<float>();
}

vector<GLfloat> Button::vertices() const{
	return position.vertices();
}

vector<GLfloat> Button::coordinates() const{
	switch(state){
	case 1: 		 return highlighted.coordinates();
	case 2: 		 return pressed.coordinates();
	case 0: default: return idled.coordinates();
	}
}

void Button::print() const{
	font.FaceSize(12);
	FTBBox dimensions = font.BBox(text.c_str());
    float sw = Options::get<unsigned>(Options::WIDTH);
    float sh = Options::get<unsigned>(Options::HEIGHT);
	float dw = ((position.width()*0.5*sw) - (dimensions.Upper().X() - dimensions.Lower().X()))/2;
	float dh = ((position.height()*0.5*sh) - (dimensions.Upper().Y() - dimensions.Lower().Y()))/2;
	float xp = (position.get(0).x*0.5+0.5)*sw;
	float yp = (position.get(0).y*0.5+0.5)*sh;
	float x = xp + dw;
	float y = yp + dh;

	glColor3d(r,g,b);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glOrtho(0,sw,0,sh,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	font.Render(text.c_str(), -1, FTPoint(x,y));

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}
