/*
 * Panel.cpp
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#include "Panel.h"
using namespace std;

Panel::Panel(string panel): Element({"Panels", panel}), sprite(file, {"Panels", panel}) {}

vector<float> Panel::coordinates() const{
	return sprite.coordinates();
}
