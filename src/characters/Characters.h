/*
 * Characters.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef CHARACTERS_H_
#define CHARACTERS_H_

#include "../rendering/Painter.h"
#include "Player.h"
#include <vector>
#include <string>

class Characters : public Painter{
public:
	Characters(Indicator& indicator);
	Player& player();
	void process();
};

#endif /* CHARACTERS_H_ */
