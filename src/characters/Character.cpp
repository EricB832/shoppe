/*
 * Character.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include "../Game.h"
#include "Character.h"
#include "../shop/Square.h"
#include <yaml-cpp/yaml.h>
#include <glm/glm.hpp>

using namespace std;

const string Character::PATH = "data/info/characters.yml";
const string Character::ROOT = "Characters";
const string Character::SPEED = "Speed";

const YAML::Node Character::file = YAML::LoadFile(PATH);

Character::Character(string name, float x, float y):
        dmask(240),
        velocity(0,0),
        sprite(file, vector<string>{ROOT, name}),
        position(x,y,Square::width,Square::height),
        item(nullptr){
    parse(file, ROOT, name);
}

void Character::parse(const YAML::Node& file, string root, string name){
	s = file[ROOT][name][SPEED].as<float>();
}

vector<float> combine(vector<float> v1, vector<float> v2) {
	vector<float> v;
	v.insert(v.end(), v1.begin(), v1.end());
	v.insert(v.end(), v2.begin(), v2.end());
	return v;
}

vector<float> Character::vertices() const{
    return position.vertices();
}

vector<float> Character::coordinates() const{
    return sprite.coordinates();
}

void Character::stop(int d){
	flag(d<<4);
	unflag(d);
	if (!moving()){
		flag(d);
	}
}

void Character::go(int d){
	clear(~d & 0x0F);
	flag(d);
	unflag(d<<4);
}

void shrink(float& d, float v){
	if(d>0)d=(d-v)>0?d-v:0;
	if(d<0)d=(d+v)<0?d+v:0;
}

void Character::accelerate(int d){
	double theta = 3.1415926539/2. - (double)d*3.1415926539/4.;
	glm::vec2 acc(cos(theta),sin(theta));
	acc *= (Game::delta()*s);
	velocity = acc;
}

int Character::process(){
	if(moving()) {
		change = true;
		accelerate(direction());
		position.translate(velocity.x, velocity.y);
	}
	position.rotate(45*direction()+180);
	return 2;
}

void Character::clear(int d){
	if ((d&UP) && (dmask & R_UP)) unflag(UP);
	if ((d&LEFT) && (dmask & R_LEFT)) unflag(LEFT);
	if ((d&DOWN) && (dmask & R_DOWN)) unflag(DOWN);
	if ((d&RIGHT) && (dmask & R_RIGHT)) unflag(RIGHT);
}

bool Character::can_hold(Item* item){
    if(item && !this->item){
        return true;
    }
    return false;
}

void Character::hold(Item* item){
    this->item = item;
}

void Character::flag(int d){
	dmask |= d;
}

void Character::unflag(int d){
	dmask &= ~d;
}
bool Character::moving(){
	return (dmask & 0xF0) != 0xF0;
}

int Character::direction(){
	if((dmask & UP) && (dmask & RIGHT)) return 1;
	if((dmask & UP) && (dmask & LEFT)) return 7;
	if (dmask & UP) return 0;
	if((dmask & DOWN) && (dmask & RIGHT)) return 3;
	if((dmask & DOWN) && (dmask & LEFT)) return 5;
	if (dmask & DOWN) return 4;
	if (dmask & RIGHT) return 2;
	if (dmask & LEFT) return 6;
	return 4;
}

Cell Character::facing(){
	int d = direction();
	double e = 0.8;
	Point p(0,0);
	switch(d){
	case 0: p = Point(position.center().x/Square::width, position.get(3).y/Square::height+e); break;
	case 1: p = Point(position.get(3).x/Square::width+e, position.get(3).y/Square::height+e); break;
	case 2: p = Point(position.get(3).x/Square::width+e, position.center().y/Square::height); break;
	case 3: p = Point(position.get(3).x/Square::width+e, position.get(0).y/Square::height-e); break;
	case 4: default: p = Point(position.center().x/Square::width, position.get(0).y/Square::height-e); break;
	case 5: p = Point(position.get(0).x/Square::width-e, position.get(0).y/Square::height-e); break;
	case 6: p = Point(position.get(0).x/Square::width-e, position.center().y/Square::height); break;
	case 7: p = Point(position.get(0).x/Square::width-e, position.get(3).y/Square::height+e); break;
	}
	return Cell(p.x>0?p.x:p.x-1, p.y>0?p.y:p.y-1);
}
