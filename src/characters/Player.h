/*
 * Player.h
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Character.h"
#include "../ui/Control.h"

class Indicator;
class Floor;
class Items;

class Player: public Character {
private:
	Indicator& indicator;
public:
	Player(std::string name, float x, float y, Indicator& indicator);
	int process();
	Control movement(int direction);
    Control use(Floor& floor, Items& items);
};

#endif /* PLAYER_H_ */
