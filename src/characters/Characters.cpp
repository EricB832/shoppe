/*
 * Characters.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include "../shop/Square.h"
#include "../shop/Indicator.h"
#include "Characters.h"
#include "Sid.h"
using namespace std;

Characters::Characters(Indicator& indicator):Painter("data/art/characters.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"){
	strokes.push_back(move(unique_ptr<Character>(new Sid(indicator))));
}

Player& Characters::player(){
	return dynamic_cast<Player&>(*strokes[0].get());
}
