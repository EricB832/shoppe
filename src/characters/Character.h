/*
 * Character.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"
#include "../rendering/Stroke.h"
#include "../shop/Item.h"
#include <glm/glm.hpp>

enum Direction {
	UP=1,
	RIGHT=2,
	DOWN=4,
	LEFT=8,
};
enum Release{
	R_UP=16,
	R_RIGHT=32,
	R_DOWN=64,
	R_LEFT=128
};

struct Cell{
	Cell(int i, int j):i(i),j(j){}
	int i,j;
};

class Character : public Stroke{
private:
	static const YAML::Node file;
	static const std::string PATH,ROOT,SPEED;
	void parse(const YAML::Node& file, std::string root, std::string name);
	void flag(int d);
	void unflag(int d);
	void clear(int d);
	bool moving();
	void accelerate(int d);
	void decelerate();
	int dmask;
	float s;

	glm::vec2 velocity;
public:
	Character(std::string name, float x, float y);

	Sprite sprite;
	Quad position;
    Item* item;

	std::vector<float> vertices()const;
	std::vector<float> coordinates()const;

    bool can_hold(Item* item);
    void hold(Item* item);

	virtual int process();
	void stop(int d);
	void go(int d);
	int direction();
	Cell facing();
};

#endif /* CHARACTER_H_ */
