/*
 * Sid.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#include "Sid.h"
#include "../shop/Square.h"

Sid::Sid(Indicator& indicator): Player("Sid", 1*Square::width, 1*Square::height, indicator){}

