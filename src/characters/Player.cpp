/*
 * Player.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#include "Player.h"
#include "../shop/Indicator.h"
#include "../shop/Floor.h"
#include "../shop/Items.h"
#include <Gl/glfw3.h>

Player::Player(std::string name, float x, float y, Indicator& indicator):Character(name,x,y),indicator(indicator) {}

int Player::process(){
	Cell c = facing();
	indicator.move(c.i,c.j);
	return Character::process();
}

char key(int direction){
	return 	direction == UP ? 'W' :
			direction == LEFT ? 'A' :
			direction == DOWN ? 'S' : 'D';

}

Control Player::movement(int direction) {
	return Control(key(direction), [this, direction](){this->go(direction);}, nullptr, [this, direction](){this->stop(direction);});
}

Control Player::use(Floor& floor, Items& items){
	return Control(GLFW_KEY_SPACE,
            [&floor, &items, this](){
                Cell c = this->facing();
				auto object = floor.get(c.i, c.j).object.get();
				if(object){
                    auto item = object->activate(position);
                    if(this->can_hold(item.get())){
                        this->hold(item.get());
                        items.add(move(item));
                        items.sketch();
                    }
				}
			}, nullptr, nullptr);
}
