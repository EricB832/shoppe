/*
 * Sid.h
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#ifndef SID_H_
#define SID_H_

#include "Player.h"
#include "../shop/Indicator.h"

class Sid : public Player{
public:
	Sid(Indicator& indicator);
};

#endif /* SID_H_ */
