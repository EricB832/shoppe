/*
 * Error.cpp
 *
 *  Created on: Feb 20, 2013
 *      Author: eric
 */

#include "Error.h"
#include <GL/glew.h>
#include <iostream>

void Error::print(GLenum error) {
	if(error == GL_INVALID_ENUM){
		std::cout << "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag." << std::endl;
	} else if (error == GL_INVALID_VALUE){
		std::cout << "GL_INVALID_VALUE: A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag." << std::endl;
	} else if (error == GL_INVALID_OPERATION){
		std::cout << "GL_INVALID_OPERATION: The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag." << std::endl;
	} else if (error == GL_INVALID_FRAMEBUFFER_OPERATION){
		std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: The frame buffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag." << std::endl;
	} else if (error == GL_OUT_OF_MEMORY){
		std::cout << "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded." << std::endl;
	} else if (error == GL_STACK_OVERFLOW){
		std::cout << "GL_STACK_OVERFLOW: An attempt has been made to perform an operation that would cause an internal stack to overflow." << std::endl;
	} else if (error == GL_STACK_UNDERFLOW){
		std::cout << "GL_STACK_UNDERFLOW: An attempt has been made to perform an operation that would cause an internal stack to underflow." << std::endl;
	}
}

void Error::printAll(){
	GLenum error = glGetError();
	while(error != GL_NO_ERROR){
		print(error);
		error = glGetError();
	}
}
