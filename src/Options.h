/*
 * Options.h
 *
 *  Created on: Feb 18, 2013
 *      Author: eric
 */

#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <map>
#include <string>
#include <vector>
#include <map>
#include <yaml-cpp/yaml.h>

class Options {
private:
    static const YAML::Node file;

public:
	static const std::string WIDTH, HEIGHT, MENU_FONT;

    template<typename T>
    static T get(std::string option){
        if(file[option.c_str()])
            return file[option.c_str()].as<T>();
        return T();
    }

};

#endif /* OPTIONS_H_ */
