/*
 * Portal.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */
#include "Portal.h"
#include "Item.h"
#include "../rendering/Stroke.h"
#include "../rendering/Quad.h"
#include "../ui/Panel.h"
#include "../ui/Tab.h"
#include "../Options.h"
#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <string>
using namespace std;

vector<unique_ptr<Stroke>> menu_elements(Menu& menu, string era){
	vector<unique_ptr<Stroke> > elements;
	elements.emplace_back(new Panel(era + " Calibrate" ));

	elements.emplace_back(new Tab("Military"));
	elements.emplace_back(new Tab("Medicine"));
	elements.emplace_back(new Tab("Technology"));
	elements.emplace_back(new Tab("Luxury"));
	elements.emplace_back(new Tab("Furniture"));

	((Tab*)elements[1].get())->selected = true;

	return elements;
}

vector<unique_ptr<Stroke>> menu_items(Menu& menu, string era){
	vector<unique_ptr<Stroke> > items;
	auto buttons = menu.buttons();
	auto available = Item::items(era, Menu::tabs[era]);
	for (unsigned i = 0; i < min(buttons.size(), available.size()); i++){
		items.emplace_back(new Item(buttons[i]->position.shrink(8), era, Menu::tabs[era], available[i].name));
	}
	return items;
}

void Portal::calibrate(Menu& menu, Items& items, string era){
	auto menu_strokes = menu_elements(menu, era);
	menu.sketch(move(menu_strokes));

	auto item_strokes = menu_items(menu, era);
	items.sketch(move(item_strokes));

	items.transform(glm::mat4());
}

unique_ptr<Item> Portal::activate(Quad position){
    cout << "Activate!" << endl;
    return unique_ptr<Item>(new Item(position, "Tribal", "Military", "Amulet"));
}
