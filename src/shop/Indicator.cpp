/*
 * Indicator.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#include "Indicator.h"
#include "../characters/Character.h"
#include "Square.h"
#include <vector>
#include <string>
#include <iostream>
using namespace std;

Indicator::Indicator(int i, int j):Square(i, j, unique_ptr<Tile>(new Tile("Indicator"))){}

void Indicator::move(int i, int j){
	change = true;
	position.move(i*Square::width, j*Square::height);
}
int Indicator::process() {
	return 2;
}
