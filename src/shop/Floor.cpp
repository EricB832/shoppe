/*
 * Floor.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#include "Floor.h"
#include "Indicator.h"
#include "Portal.h"
#include "../ui/Panel.h"
#include <stdexcept>
#include <string>
#include <memory>
#include <stdexcept>
using namespace std;

Floor::Floor(int w, int h):Painter(	"data/art/furniture.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"),width(w),height(h){
	if(w<=2 || h<=2) throw new std::logic_error("Floor is too small.");
	for(int i = 0; i < w*h; i++){
		strokes.push_back(move(unique_ptr<Square>(new Square(i%w, i/w, unique_ptr<Tile>(new Tile("Void"))))));
	}
	portals();
	indicator();
}

void Floor::portals(){
	// Entrance portals
	strokes.emplace_back(new Square(-1, height/2, 		unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Tribal"))));
	strokes.emplace_back(new Square(width/4, height, 	unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Iron"))));
	strokes.emplace_back(new Square((3*width)/4, height,unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Dark"))));
	strokes.emplace_back(new Square(width, height/2, 	unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Renaissance"))));
	strokes.emplace_back(new Square((3*width)/4, -1, 	unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Information"))));
	strokes.emplace_back(new Square(width/4, -1, 		unique_ptr<Tile>(new Tile("Void")), unique_ptr<Furniture>(new Portal("Space"))));

	// Indicator
	strokes.emplace_back(new Indicator(width/2, height/2));
}

Indicator& Floor::indicator(){
	return *(Indicator*)strokes.back().get();
}

bool Floor::valid(int i, int j){
	if(i >= 0 && j >= 0 && i < width && j < height){
		return true;
	} else if (i==-1 && j==height/2) {
		return true;
	} else if (i==width/4 && j==height) {
		return true;
	} else if (i==3*width/4 && j==height) {
		return true;
	} else if (i==width && j==height/2) {
		return true;
	} else if (i==3*width/4 && j==-1) {
		return true;
	} else if (i==width/4 && j==-1) {
		return true;
	}
	return false;
}

Square& Floor::get(int i, int j){
	if(i >= 0 && j >= 0 && i < width && j < height){
		return *dynamic_cast<Square*>(strokes[j*width+i].get());
	} else if (i==-1 && j==height/2) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-7].get());
	} else if (i==width/4 && j==height) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-6].get());
	} else if (i==3*width/4 && j==height) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-5].get());
	} else if (i==width && j==height/2) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-4].get());
	} else if (i==3*width/4 && j==-1) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-3].get());
	} else if (i==width/4 && j==-1) {
		return *dynamic_cast<Square*>(strokes[strokes.size()-2].get());
	} else {
		throw std::runtime_error("Floor square out of bounds.");
	}
}

unsigned Floor::pixW(){
	return width*Square::width;
}

unsigned Floor::pixH(){
	return height*Square::height;
}
