/*
 * Item.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#include "Item.h"
#include <string>
#include <vector>
using namespace std;

const YAML::Node Item::file = YAML::LoadFile("data/info/items.yml");

Item::Item(Quad position, string era, string category, string name):position(position),sprite(file, vector<string>{"Items", era, category, name}),name(name){}

vector<float> Item::coordinates() const {
	return sprite.coordinates();
}

vector<float> Item::vertices() const {
	return position.vertices();
}

vector<Item> Item::items(std::string era, std::string category) {
	vector<Item> v;
	for (auto i : file["Items"][era][category]){
		v.emplace_back(Quad(0,0,0,0), era, category, i.first.as<string>());
	}
	return v;
}
