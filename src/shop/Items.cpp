/*
 * Items.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#include "Items.h"
#include "Item.h"
using namespace std;

Items::Items():Painter("data/art/items.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"){}

void Items::pop(){
	clear();
}

void Items::add(unique_ptr<Item> item){
    strokes.push_back(move(item));
}
