/*
 * Items.h
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#ifndef ITEMS_H_
#define ITEMS_H_

#include "../rendering/Painter.h"
#include <map>
#include <vector>
#include <string>
#include "Item.h"

enum Category{
	Military,
	Medicine,
	Technology,
	Luxury,
    Furnishing
};

class Items: public Painter {
public:
	Items();
	void pop();
    void add(std::unique_ptr<Item> item);

	static std::map<std::string, std::vector<std::string> > load();
};

#endif /* ITEMS_H_ */
