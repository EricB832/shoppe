/*
 * Square.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Tile.h"
#include "Furniture.h"
#include "../rendering/Stroke.h"
#include "../rendering/Quad.h"
#include <memory>

class Furniture;
class Tile;

class Square : public Stroke{
public:

	static const float width;
	static const float height;

	Quad position;
	std::unique_ptr<Tile> 		floor  = nullptr;
	std::unique_ptr<Furniture>	object = nullptr;

	Square(int i, int j, std::unique_ptr<Tile> floor, std::unique_ptr<Furniture> object);
	Square(int i, int j, std::unique_ptr<Tile> floor);
	std::vector<float> vertices() const;
	std::vector<float> coordinates() const;
	virtual int process(){return 0;}
};

#endif /* SQUARE_H_ */
