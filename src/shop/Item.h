/*
 * Item.h
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#ifndef ITEM_H_
#define ITEM_H_

#include "../rendering/Stroke.h"
#include "../rendering/Quad.h"
#include "../rendering/Sprite.h"
#include <yaml-cpp/yaml.h>

class Item : public Stroke{
protected:
	static const YAML::Node file;
	Quad position;
	Sprite sprite;
public:
	Item(Quad position, std::string era, std::string category, std::string name);
	std::vector<float> coordinates() const;
	std::vector<float> vertices() const;

	std::string name;
	static std::vector<Item> items(std::string era, std::string category);
};

#endif /* ITEM_H_ */
