/*
 * Portal.h
 *
 *  Created on: Jun 7, 2013
 *      Author: Eric
 */

#ifndef PORTAL_H_
#define PORTAL_H_

#include "Furniture.h"
#include "../ui/Menu.h"
#include "Items.h"

class Portal : public Furniture{
public:
	Portal(std::string era):Furniture({"Furniture", era, "Portal"}),era(era){}
	std::string era;
	void calibrate(Menu&, Items&, std::string);
    std::unique_ptr<Item> activate(Quad position);
};

#endif /* PORTAL_H_ */
