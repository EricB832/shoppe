/*
 * Furniture.cpp
 *
 *  Created on: Mar 14, 2013
 *      Author: Eric
 */

#include "Furniture.h"
#include "../shop/Square.h"
#include <string>
using namespace std;

const YAML::Node Furniture::file = YAML::LoadFile("data/info/furniture.yml");

Furniture::Furniture(vector<string> name):sprite(file, name){}
