/*
 * Indicator.h
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#ifndef INDICATOR_H_
#define INDICATOR_H_

#include "../shop/Square.h"
#include <GL/glew.h>
#include <vector>

class Indicator : public Square {
public:
	Indicator(int i, int j);
	void move(int i, int j);
	int process();
};

#endif /* INDICATOR_H_ */
