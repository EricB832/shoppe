/*
 * Furniture.h
 *
 *  Created on: Mar 14, 2013
 *      Author: Eric
 */

#ifndef FURNITURE_H_
#define FURNITURE_H_

#include <string>
#include <vector>
#include <memory>
#include <yaml-cpp/yaml.h>
#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"
#include "../shop/Item.h"

class Furniture {
public:
	Furniture(std::vector<std::string> name);

	Sprite sprite;

    virtual std::unique_ptr<Item> activate(Quad){return nullptr;}

	virtual ~Furniture(){}
private:
	static const YAML::Node file;
};

#endif /* FURNITURE_H_ */
