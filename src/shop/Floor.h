/*
 * Floor.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef FLOOR_H_
#define FLOOR_H_

#include "Square.h"
#include "../rendering/Painter.h"
#include "Indicator.h"
#include "../Error.h"

class Square;
class Indicator;

class Floor : public Painter{
private:
	void portals();
	int width;
	int height;
public:
	unsigned pixW();
	unsigned pixH();
	Floor(int w, int h);
	Square& get(int i, int j);
	bool valid(int i, int j);
	Indicator& indicator();

};

#endif /* FLOOR_H_ */
