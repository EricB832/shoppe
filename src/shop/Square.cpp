/*
 * Square.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#include "Square.h"
#include <vector>
using namespace std;

const float Square::width=64;
const float Square::height=64;

Square::Square(int i, int j, unique_ptr<Tile> floor, unique_ptr<Furniture> object):
		position(i*width,j*height,width,height),
		floor(move(floor)),
		object(move(object)){}
Square::Square(int i, int j, unique_ptr<Tile> floor):Square(i,j,move(floor),nullptr){}

std::vector<GLfloat> Square::vertices() const{
	vector<GLfloat> v1 = position.vertices();
	vector<GLfloat> v; v.reserve(v1.size()*2);
	v.insert(v.end(), v1.begin(), v1.end());
	if(object)v.insert(v.end(), v1.begin(), v1.end());
	return v;
}

std::vector<GLfloat> Square::coordinates() const{
	vector<GLfloat> t = floor->sprite.coordinates();
	vector<GLfloat> f = object?object->sprite.coordinates():vector<GLfloat>();
	vector<GLfloat> r; r.reserve(t.size()+f.size());
	r.insert(r.end(), t.begin(), t.end());
	r.insert(r.end(), f.begin(), f.end());
	return r;
}
