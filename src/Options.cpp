#include <yaml-cpp/yaml.h>
#include "Options.h"

const std::string Options::WIDTH = "Width";
const std::string Options::HEIGHT = "Height";
const std::string Options::MENU_FONT = "Menu Font";

const YAML::Node Options::file = YAML::LoadFile("data/init.yml");
