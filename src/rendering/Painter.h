/*
 * Painter.h
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#ifndef PAINTER_H_
#define PAINTER_H_

#include <vector>
#include <string>
#include <map>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <memory>
#include "../ui/Control.h"
#include "Stroke.h"
#include "Shader.h"

class Painter {
public:
	Painter(std::string texture, std::string vertex, std::string fragment);
	Painter(const Painter&)=delete;
	Painter(Painter&&)=default;
	void updateSubTextureBuffer(std::vector<GLfloat> const& coordinates, GLuint index);
	void updateSubVertexBuffer(std::vector<GLfloat> const& coordinates, GLuint index);
	void paint() const;
	void clear();

	//Painting
	void sketch();
	void sketch(std::vector<std::unique_ptr<Stroke> > strokes);
	void transform(glm::mat4 m);
	void process(int updates, Stroke& stroke, unsigned index);
	void update();
	std::vector<GLfloat> vertices()    const;
	std::vector<GLfloat> coordinates() const;
	std::vector<GLuint>  elements()    const;
	std::string texture;
	std::string vertex;
	std::string fragment;
protected:
	std::vector<std::unique_ptr<Stroke> > strokes;
	std::unique_ptr<glm::mat4> matrix;
private:
	static const std::string UNIFORM_MATRIX, UNIFORM_TEXTURE, ATTRIBUTE_XY, ATTRIBUTE_ST;
	void getIDs();
	void buildTexture(std::string tex);
	void buildShader(std::string vertex, std::string fragment);
	void buildBuffers(std::vector<GLfloat> vertices, std::vector<GLfloat> coordinates, std::vector<GLuint> elements);

	bool render;

	GLuint texID;

	GLuint bufferVertices;
	GLuint bufferCoordinates;
	GLuint bufferElements;

	GLint attributeVertices;
	GLint attributeCoordinates;

	GLint uniformTexture;
	GLint uniformMatrix;

	Shader shader;
};

#endif /* PAINTER_H_ */
