/*
 * Painter.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#include "Painter.h"
#include "../Error.h"
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>
#include <string>
#include <iostream>
#include <vector>
using namespace std;
const std::string Painter::UNIFORM_MATRIX = "matrix";
const std::string Painter::UNIFORM_TEXTURE = "texture";
const std::string Painter::ATTRIBUTE_XY = "xy";
const std::string Painter::ATTRIBUTE_ST = "st";

Painter::Painter(std::string texture, std::string vertex, std::string fragment):texture(texture),vertex(vertex),fragment(fragment),
		matrix(unique_ptr<glm::mat4>(new glm::mat4)), render(false){}

void Painter::getIDs(){
	attributeVertices = glGetAttribLocation(shader.getProgram(), ATTRIBUTE_XY.c_str());
	attributeCoordinates = glGetAttribLocation(shader.getProgram(), ATTRIBUTE_ST.c_str());
	uniformMatrix = glGetUniformLocation(shader.getProgram(), UNIFORM_MATRIX.c_str());
	uniformTexture = glGetUniformLocation(shader.getProgram(), UNIFORM_TEXTURE.c_str());
	Error::printAll();
}

GLuint getBuffer(GLenum target, const void* data, GLsizei size){
    GLuint b;
    glGenBuffers(1, &b);
    glBindBuffer(target, b);
	glBufferData(target, size, data, GL_STATIC_DRAW);
	Error::printAll();
    return b;
}

void Painter::updateSubTextureBuffer(const std::vector<GLfloat>& c, GLuint index) {
	glBindBuffer(GL_ARRAY_BUFFER, bufferCoordinates);
	glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)(index*sizeof(GLfloat)), (GLsizeiptr)(c.size()*sizeof(GLfloat)), (void*)c.data());
	Error::printAll();
}

void Painter::updateSubVertexBuffer(const std::vector<GLfloat>& v, GLuint index) {
	glBindBuffer(GL_ARRAY_BUFFER, bufferVertices);
	glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)(index*sizeof(GLfloat)), (GLsizeiptr)(v.size()*sizeof(GLfloat)), (void*)v.data());
	Error::printAll();
}

void Painter::buildBuffers(vector<GLfloat> vertices, vector<GLfloat> coordinates, vector<GLuint> elements) {
	bufferVertices = getBuffer(GL_ARRAY_BUFFER, vertices.data(), (GLsizeiptr)(vertices.size()*sizeof(GLfloat)));
	bufferCoordinates = getBuffer(GL_ARRAY_BUFFER, coordinates.data(), (GLsizeiptr)(coordinates.size()*sizeof(GLfloat)));
	bufferElements = getBuffer(GL_ELEMENT_ARRAY_BUFFER, elements.data(), (GLsizeiptr)(elements.size()*sizeof(GLuint)));
	Error::printAll();
}

void Painter::buildTexture(std::string tex){
	texID = SOIL_load_OGL_texture(tex.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
	Error::printAll();
}

void Painter::buildShader(std::string vertex, std::string fragment) {
	shader.buildShader(vertex, fragment);
	Error::printAll();
}

void Painter::paint() const{
	if(!render) return;
	glUseProgram(shader.getProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glUniform1i(uniformTexture, 0);
	glUniformMatrix4fv(uniformMatrix, 1, GL_FALSE, glm::value_ptr(*matrix));

	glBindBuffer(GL_ARRAY_BUFFER, bufferCoordinates);
	glVertexAttribPointer(attributeCoordinates, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, nullptr);
	glEnableVertexAttribArray(attributeCoordinates);

	glBindBuffer(GL_ARRAY_BUFFER, bufferVertices);
	glVertexAttribPointer(attributeVertices, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, nullptr);
	glEnableVertexAttribArray(attributeVertices);

	//Make magic!
	GLsizei size;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferElements);
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, nullptr);

	//Clean up
	glDisableVertexAttribArray(attributeCoordinates);
	glDisableVertexAttribArray(attributeVertices);
	glUseProgram(0);

	// Make sure nothing went wrong
	Error::printAll();
}

void Painter::sketch(){
	if(strokes.empty()) return;
	if(render) clear();
	render = true;
	buildBuffers(vertices(), coordinates(), elements());
	buildTexture(texture);
	buildShader(vertex,fragment);
	getIDs();
}

void Painter::sketch(vector<unique_ptr<Stroke>> strokes){
	swap(this->strokes, strokes);
	sketch();
}

vector<GLfloat> Painter::vertices()const{
	if (strokes.empty()) return vector<GLfloat>();
	vector<GLfloat> vertices;
	for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> v = (*p)->vertices();
		vertices.insert(vertices.end(), v.begin(), v.end());
	}
	return vertices;
}
vector<GLfloat> Painter::coordinates()const{
	if (strokes.empty()) return vector<GLfloat>();
	vector<GLfloat> coordinates;
	for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> c = (*p)->coordinates();
		coordinates.insert(coordinates.end(), c.begin(), c.end());
	}
	return coordinates;
}
vector<GLuint>  Painter::elements()const{
	vector<GLuint> elements;
	for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> v = (*p)->vertices();
		for(unsigned i = 0; i < v.size(); i+=2){
			elements.push_back(elements.size());
		}
	}
	return elements;
}

void Painter::process(int updates, Stroke& stroke, unsigned index){
	if(!updates || !stroke.changed()) return;
	if(updates & 1) updateSubTextureBuffer(stroke.coordinates(), index);
	if(updates & 2) updateSubVertexBuffer(stroke.vertices(), index);
}

void Painter::update() {
	unsigned index = 0;
	for (auto& stroke : strokes) {
		process(stroke->process(), *stroke.get(), index);
		index += stroke->vertices().size();
	}
}

void Painter::transform(glm::mat4 m){
	matrix.reset(new glm::mat4(m));
}


void Painter::clear(){
	if(render){
		glDeleteTextures(1, &texID);
		glDeleteBuffers(1, &bufferVertices);
		glDeleteBuffers(1, &bufferCoordinates);
		glDeleteBuffers(1, &bufferElements);
		strokes.clear();
		shader.clear();
		render = false;
	}
}

