/*
 * Sprite.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include "Sprite.h"
#include <yaml-cpp/yaml.h>
using namespace std;

const string Sprite::SPRITE = "Texture";
const string Sprite::TEX_WIDTH = "Width";
const string Sprite::TEX_HEIGHT = "Height";
const string Sprite::X = "S";
const string Sprite::Y = "T";
const string Sprite::WIDTH = "DS";
const string Sprite::HEIGHT = "DT";

Sprite::Sprite(const YAML::Node& file, const vector<string>& name){
	parse(file, name);
}

const YAML::Node get(const YAML::Node& node, const vector<string>& name, unsigned i){
	if(i == (name.size()-1)) return node[name[i]];
	return get(node[name[i]], name, i+1);
}

const YAML::Node get(const YAML::Node& node, const vector<string>& name){
	return get(node, name, 0);
}

void Sprite::parse(const YAML::Node& file, const vector<string>& name){
	tw = file[TEX_WIDTH].as<int>();
	th = file[TEX_HEIGHT].as<int>();
	const YAML::Node& n = get(file, name);
	x = n[SPRITE][X].as<int>()/tw;
	y = n[SPRITE][Y].as<int>()/th;
	width  = n[SPRITE][WIDTH ].as<int>()/tw;
	height = n[SPRITE][HEIGHT].as<int>()/th;
}

std::vector<GLfloat> Sprite::coordinates() const{
	if(tw == th && tw == 0) return vector<GLfloat>();
	return vector<GLfloat> {x,y,x+width,y,x,y+height,x+width,y,x+width,y+height,x,y+height};
}
