/*
 * Shader.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#include "Shader.h"

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cerrno>
#include <cstdlib>
#include <GL/glew.h>

std::string getFileContents(const char *filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return(contents);
  }
  throw(errno);
}

static void printLog(
    GLuint object,
    PFNGLGETSHADERIVPROC glGet__iv,
    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog
)
{
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = (char*)malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}

GLuint getShader(GLenum type, std::string file){
	std::string source = getFileContents(file.c_str());
	GLuint shader = glCreateShader(type);
	const char* s = source.c_str();
	glShaderSource(shader, 1, &s, nullptr);
	glCompileShader(shader);
	int compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if(!compiled){
		std::cout << "Failed to compile " << file << std::endl;
		printLog(shader, glGetShaderiv, glGetShaderInfoLog);
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}

GLuint makeProgram(GLuint vertex, GLuint fragment){
	GLuint program = glCreateProgram();
	glAttachShader(program, vertex);
	glAttachShader(program, fragment);
	glLinkProgram(program);

	int compiled;
	glGetProgramiv(program, GL_LINK_STATUS, &compiled);
	if(!compiled){
		std::cout << "Failed to link: " << std::endl;
		printLog(program, glGetProgramiv, glGetProgramInfoLog);
		glDeleteProgram(program);
		return 0;
	}
	glDetachShader(program, vertex);
	glDetachShader(program, fragment);
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	return program;
}

void Shader::buildShader(std::string vertex, std::string fragment){
	GLuint v = getShader(GL_VERTEX_SHADER, vertex);
	GLuint f = getShader(GL_FRAGMENT_SHADER, fragment);
	this->program = makeProgram(v, f);
}

void Shader::clear(){
	glDeleteProgram(program);
}
