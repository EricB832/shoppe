/*
 * Stroke.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef STROKE_H_
#define STROKE_H_

#include <vector>
#include <string>

class Stroke {
protected:
	bool change=false;
public:
	virtual std::vector<float> vertices   ()const=0;
	virtual std::vector<float> coordinates()const=0;
	virtual int process(){return 0;}
	bool changed(){bool c=change; change=false; return c;}
};

#endif /* STROKE_H_ */
