/*
 * Sprite.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef SPRITE_H_
#define SPRITE_H_

#include <string>
#include <vector>
#include <GL/glew.h>
#include <yaml-cpp/yaml.h>
#include <glm/glm.hpp>

class Sprite {
private:
	void parse(const YAML::Node& file, const std::vector<std::string>& name);
	static const std::string SPRITE, X, Y, WIDTH, HEIGHT, TEX_WIDTH, TEX_HEIGHT;
	float x, y, width, height, tw, th;
public:
	Sprite(const YAML::Node& file, const std::vector<std::string>& name);
	std::vector<GLfloat> coordinates() const;
	float texWidth() const{return tw;}
	float texHeight() const{return th;}
};

#endif /* SPRITE_H_ */
