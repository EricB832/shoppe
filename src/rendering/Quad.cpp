/*
 * Quad.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */

#include "Quad.h"
#include "../Options.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
using namespace std;

const string Quad::QUAD = "Position";
const string Quad::X = "X";
const string Quad::Y = "Y";
const string Quad::WIDTH = "Width";
const string Quad::HEIGHT = "Height";

Quad::Quad(float x, float y, float w, float h): p1(x,y),p2(x+w,y),p3(x,y+h),p4(x+w,y+h) {}

Quad::Quad(const YAML::Node& file, const std::vector<std::string>& name){
	parse(file, name);
}

Point Point::multiply(glm::mat4 matrix) const{
	glm::vec4 p = glm::vec4(x,y,0,1);
	glm::vec4 q = matrix*p;
	return Point(q.x/q.w, q.y/q.w);
}

vector<GLfloat> Quad::vertices() const{
	if(p1.x==p4.x && p1.y == p4.y && p1.x==0 && p1.y==0) return vector<GLfloat>();
	Point q1 = p1.multiply(matrix);
	Point q2 = p2.multiply(matrix);
	Point q3 = p3.multiply(matrix);
	Point q4 = p4.multiply(matrix);
	return vector<GLfloat> {q1.x,q1.y,q2.x,q2.y,q3.x,q3.y,q2.x,q2.y,q4.x,q4.y,q3.x,q3.y};
}

float Quad::width() const{
	return p4.x-p1.x;
}

float Quad::height() const{
	return p4.y-p1.y;
}

Point Quad::get(unsigned i) const{
	switch(i){
	case 1: return p2;
	case 2: return p3;
	case 3: return p4;
	case 0: default: return p1;
	}
}

Point Quad::center(){
	return Point((p1.x+p2.x+p3.x+p4.x)/4, (p1.y+p2.y+p3.y+p4.y)/4);
}

void Quad::rotate(float degrees){
	matrix = glm::translate<GLfloat>(glm::mat4(), glm::vec3(center().x, center().y, 0));
	matrix = glm::rotate<GLfloat>(matrix, degrees, glm::vec3(0,0,-1));
	matrix = glm::translate<GLfloat>(matrix, glm::vec3(-center().x,-center().y,0));
}

void Quad::transform(glm::mat4 matrix){
	this->matrix = matrix;
}

void Quad::translate(float x, float y){
	p1.move(x,y);
	p2.move(x,y);
	p3.move(x,y);
	p4.move(x,y);
}
void Quad::move(float x, float y){
	float w = width();
	float h = height();
	p1 = Point(x,y);
	p2 = Point(x+w,y);
	p3 = Point(x, y+h);
	p4 = Point(x+w, y+h);
}

const YAML::Node read(const YAML::Node& node, const vector<string>& name, unsigned i){
	if(i == (name.size()-1)) return node[name[i]];
	return read(node[name[i]], name, i+1);
}

const YAML::Node read(const YAML::Node& node, const vector<string>& name){
	return read(node, name, 0);
}

void Quad::parse(const YAML::Node& file, const std::vector<std::string>& name) {
	float x,y,w,h;
	const YAML::Node& n = read(file, name);
	for (auto s : name) cout << s << " "; cout << endl;
	x = n[QUAD][X].as<float>();
	y = n[QUAD][Y].as<float>();
	w = n[QUAD][WIDTH ].as<float>();
	h = n[QUAD][HEIGHT].as<float>();
	p1 = Point(x,y);
	p2 = Point(x+w,y);
	p3 = Point(x, y+h);
	p4 = Point(x+w, y+h);
}

Quad Quad::shrink(float margin) {
	double pixw = 2./Options::get<unsigned>("Width");
	double pixh = 2./Options::get<unsigned>("Height");
	return Quad(p1.x+margin*pixw, p1.y+margin*pixh, width()-2*margin*pixw, height()-2*margin*pixh);
}
