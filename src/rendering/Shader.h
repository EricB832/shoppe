/*
 * Shader.h
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#ifndef SHADER_H_
#define SHADER_H_

#include <string>
#include <vector>
#include <map>
#include <GL/glew.h>

class Shader {
public:
	Shader()=default;
	Shader(const Shader&)=delete;
	Shader(Shader&&)=default;
	void buildShader(std::string vertex, std::string fragment);
	GLuint getProgram() const{return program;}
	void clear();
private:
	GLuint program;
};

#endif /* SHADER_H_ */
