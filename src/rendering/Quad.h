/*
 * Quad.h
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */

#ifndef QUAD_H_
#define QUAD_H_

#include <glm/glm.hpp>
#include <vector>
#include <GL/glew.h>
#include <yaml-cpp/yaml.h>
struct Point{
	float x,y;
	Point():x(0),y(0){}
	Point(float x, float y):x(x),y(y){}
	void move(float x, float y){this->x+=x; this->y+=y;}
	Point multiply(glm::mat4 matrix) const;
};

class Quad {
private:
	static const std::string QUAD, X, Y, WIDTH, HEIGHT;
	Point p1,p2,p3,p4;
	glm::mat4 matrix;

	void parse(const YAML::Node& file, const std::vector<std::string>& name);
public:
	Quad(float x, float y, float w, float h);
	Quad(const YAML::Node& file, const std::vector<std::string>& name);
	void transform(glm::mat4 matrix);
	void rotate(float degrees);
	void translate(float x, float y);
	void move(float x, float y);
	std::vector<GLfloat> vertices() const;
	float width() const;
	float height() const;
	Point get(unsigned i) const;
	Point center();
	Quad shrink (float margin);
};

#endif /* QUAD_H_ */
